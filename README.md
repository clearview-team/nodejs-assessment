# Node.js module development

Create a very simple Node.js module which must act both as an ES6 module and a regular Node.js.

Users of the library must be able to achieve the following:

```
// for ES6
import { componentName } from 'library-name';

// for non-ES6 modules
const componentName = require('library-name').componentName;
```

For this specific case, your Node.js module must do the following:

 - Fetch XML file: https://www.w3schools.com/xml/plant_catalog.xml
 - Unserialize the incoming XML into an array of objects.
 - Lowercase all keys.
 - Create methods: `getByName, getByCategory, getByZone, getByLight, getByPrice, getByAvailability` to filter out by fields: `COMMON, BOTANICAL, ZONE, LIGHT, PRICE, AVAILABILITY`, respectively.
 - Each method must return either a single matching object, or an array of matching objects.
 - Create a method `saveTo(filename: string)` which will save the last fetch into a specified JSON file.
 - `filename` must end with `.json`, must not exist already, and cannot be empty (no appending)
 - If `filename` is omited, use `plant_catalog-{{UNIX_EPOCH}}.json` filename structure
 - Must be installable as an independent `npm` package but only via Git, and not as a `npmjs.org` package.
 - API documentation (auto-generated)

Special bonuses:

 - Develop a web interface for interatcing with all the methods, incl. save
 - Allow JSON file to be downlaoded after created
 - Dockerize if possible (?)
 - Create a more general object serialization from different XML files, other than one we specified.
